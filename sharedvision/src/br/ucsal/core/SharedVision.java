package br.ucsal.core;

import java.awt.AWTException;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.IOException;

import javax.swing.JOptionPane;

import org.apache.log4j.Logger;

import br.ucsal.communication.client.Client;
import br.ucsal.communication.client.ThreadClient;
import br.ucsal.communication.server.Listener;
import br.ucsal.gui.ScreenVisualizer;
import br.ucsal.gui.SysTray;
import br.ucsal.ldap.LDAPObject;
import br.ucsal.ldap.LDAPService;
import br.ucsal.properties.OperationModeEnum;
import br.ucsal.properties.ParameterTypeEnum;
import br.ucsal.screen.ScreenService;
import br.ucsal.util.TimerService;

/**
 * Execu��o start javaw -jar sharedvision-2.0.1.jar -MODE:STUDENT -HOST:<IP-HOST>
 * 
 * @author antoniocp
 *
 */

public class SharedVision {

	private OperationModeEnum operationMode = OperationModeEnum.STUDENT;

	private Integer teacherServerPort = 2000;

	// FIXME Parametrizar essa porta.
	private Integer studentServerPort = 2001;

	private static SharedVision instance = null;

	public static Dimension screenSize;

	public static void main(String[] args) {
		screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		new SharedVision(args);
	}

	public SharedVision(String[] args) {
		instance = this;
		initParameters(args);
		start();
	}

	public static SharedVision getInstance() {
		return instance;
	}

	public OperationModeEnum getOperationMode() {
		return operationMode;
	}

	public Integer getStudentServerPort() {
		return studentServerPort;
	}

	// FIXME Voltar a tratar a comunicação bidirecional para que o professor possa ver a máquina do estudante.
	private void start() {
		if (OperationModeEnum.STUDENT.equals(this.operationMode)) {
			startClient();
		} else {
			startServer();
			SysTray sysTray = new SysTray();
			try {
				sysTray.createSysTray();
			} catch (IOException e) {
				logger.error("SysTray startup failed.");
			}
		}
	}

	private void startServer() {
		logger.info("Starting server...");
		try {
			ScreenService screenService = new ScreenService();
			screenService.start();
			TimerService.sleep(1000);
			if (OperationModeEnum.TEACHER.equals(operationMode)) {
				new Listener(screenService, teacherServerPort).start();
			} else {
				new Listener(screenService, studentServerPort).start();
			}
			logger.info("Server started.");
		} catch (AWTException e) {
			logger.error("Start server failed:" + e.getMessage());
		}
	}

	private void startClient() {
		String[] teacherIpAndPort = JOptionPane.showInputDialog("Informe o endereço do servidor:").split(":");
		String teacherHost = teacherIpAndPort[0].trim();
		if (teacherIpAndPort.length > 1) {
			teacherServerPort = Integer.parseInt(teacherIpAndPort[1].trim());
		}
		ScreenVisualizer screenVisualizer = new ScreenVisualizer();
		Client client = identifyClient();
		new ThreadClient(teacherHost, teacherServerPort, screenVisualizer, client).start();
	}

	private Client identifyClient() {
		String clientIdentification;
		LDAPObject ldapObject = new LDAPService().getLDAP();
		if (ldapObject != null) {
			clientIdentification = ldapObject.getCn();
		} else {
			clientIdentification = System.getProperty("user.name");
		}
		return new Client(clientIdentification);
	}

	private void initParameters(String[] parameters) {
		logger.debug("Initializing parameters...");
		for (String parameter : parameters) {
			try {
				String name = parameter.split(":")[0].substring(1).toUpperCase();
				String value = parameter.split(":")[1];
				setParameter(name, value);
			} catch (ArrayIndexOutOfBoundsException e) {
				logger.error("Parameter initialize failed: " + e.getMessage());
			}
		}
		logger.debug("Parameters initialized.");
		initMissingParameters();
	}

	private void initMissingParameters() {
		logger.debug("Initializing default parameters...");
		logger.debug("Default parameters initialized.");
	}

	private void setParameter(String name, String value) {
		try {
			ParameterTypeEnum type = ParameterTypeEnum.valueOf(name.toUpperCase());
			setParameter(type, value.toUpperCase());
		} catch (IllegalArgumentException e) {
			logger.error("Invalid parameter type: " + name);
		}
	}

	private void setParameter(ParameterTypeEnum type, String value) {
		switch (type) {
		case MODE:
			this.operationMode = OperationModeEnum.valueOf(value);
			break;
		case PORT:
			this.teacherServerPort = Integer.valueOf(value);
			break;
		default:
			break;
		}

	}

	private static final Logger logger = Logger.getLogger(SharedVision.class);
}

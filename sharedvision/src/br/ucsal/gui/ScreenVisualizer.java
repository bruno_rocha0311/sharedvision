package br.ucsal.gui;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;

import br.ucsal.core.SharedVision;
import br.ucsal.properties.OperationModeEnum;
import br.ucsal.properties.PropertiesService;

public class ScreenVisualizer extends JPanel {

	private static final long serialVersionUID = 1L;

	private BufferedImage screenImage;

	private String title = "SharedVision Client";

	private Dimension contentPaneSize = SharedVision.screenSize;

	private JScrollPane scrollPane;

	private static JFrame jFrame;

	public ScreenVisualizer() {
		Runnable r = () -> {
			try {
				if (OperationModeEnum.TEACHER.equals(SharedVision.getInstance().getOperationMode())) {
					createTeacherScreenVisualizer();
				} else {
					createStudentScreenVisualizer();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}
		};
		SwingUtilities.invokeLater(r);
	}

	public ScreenVisualizer(String serverIdentification) {
		this();
		this.title += " - " + serverIdentification;
	}

	public void createTeacherScreenVisualizer() {
		jFrame = new JFrame(PropertiesService.getSystemFullIdentification());
		jFrame.setTitle(title);
		jFrame.setLocation(0, 0);
		jFrame.setPreferredSize(new Dimension(600, 600));
		jFrame.getContentPane().add(this);
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jFrame.pack();
		jFrame.setVisible(true);
	}

	public void createStudentScreenVisualizer() {
//		KeyHook.blockWindowsKey();

//		new Thread() {
//			@Override
//			public void run() {
//				TimerService.sleep(20000);
//				KeyHook.unblockWindowsKey();
//			};
//		}.start();

		jFrame = new JFrame(PropertiesService.getSystemFullIdentification());

		jFrame.setLocationRelativeTo(null);
//		jFrame.setUndecorated(true);
//		jFrame.setAlwaysOnTop(true);
		jFrame.setLocationByPlatform(true);
//		jFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		GraphicsDevice d = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice();
//		d.setFullScreenWindow(jFrame);
//		jFrame.setSize(getMaximumSize());
		jFrame.setPreferredSize(SharedVision.screenSize);
		scrollPane = new JScrollPane(this);
		jFrame.getContentPane().add(scrollPane);
		jFrame.pack();
		jFrame.setVisible(true);
		contentPaneSize = jFrame.getContentPane().getSize();
		contentPaneSize = SharedVision.screenSize;//new Dimension((int) contentPaneSize.getWidth(), (int) contentPaneSize.getHeight());
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(contentPaneSize);
	}

	public static JFrame getFrame() {
		return jFrame;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (screenImage != null) {
			g.drawImage(screenImage, 0, 0, null);
		}
	}

	public void setScreenImage(BufferedImage screenImage) {
		this.screenImage = screenImage;
		this.repaint();
	}

}
